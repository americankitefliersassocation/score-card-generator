import IndexController from "./controllers/index";
import BalletController from "./controllers/ballet";
import FieldDirectorReferenceController from "./controllers/field-director-reference";
import IndoorSingleLineController from "./controllers/indoor-single-line";
import IndoorUnlimitedController from "./controllers/indoor-unlimited";
import OutdoorUnlimitedController from "./controllers/outdoor-unlimited";
import PrecisionController from "./controllers/precision";

/**
 * Initialize the app by finding the relevant controller and calling init().
 */
$(function() {
  // Mapping of routes to controllers
  const routes = {
    "/": IndexController,
    "/templates/ballet.html": BalletController,
    "/templates/field-director-reference.html": FieldDirectorReferenceController,
    "/templates/indoor-single-line.html": IndoorSingleLineController,
    "/templates/indoor-unlimited.html": IndoorUnlimitedController,
    "/templates/outdoor-unlimited.html": OutdoorUnlimitedController,
    "/templates/precision.html": PrecisionController
  };

  // Run the controller for the current url path
  const controller = routes[window.location.pathname];
  controller.init();
});
