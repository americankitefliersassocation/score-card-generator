# Score Card Generator

## Prerequisites

This project requires node/npm.

## Contributing

Master branch is restricted to Pull Requests only. Feel free to contribute by creating a branch to store your work, and submit your changes by creating a pull request. The build must be passing, and the PR must have at least 1 approval to be merged.

## Developing Locally

Download the git repo and run `npm install`. Run `npm run start` to start a local server and run the site.

### Linting

Files are managed using Prettier.

### Dependencies

The following tools are used in this package:
- Babel - Transpiles modern JS to broaden browser support
- Less - Transpiles Less into CSS
- Grunt - Manages local development, build, and deployment

## Deploying Changes

Every time a change is made to the master branch (pull request) in the BitBucket repository, the server will update and serve the latest version of Master branch. Build and deployment is done by BitBucket Pipelines.
