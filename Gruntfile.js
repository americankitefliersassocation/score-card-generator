module.exports = function(grunt) {
  require("load-grunt-tasks")(grunt);

  grunt.initConfig({
    pkg: grunt.file.readJSON("package.json"),
    "http-server": {
      dev: {
        // the server root directory
        root: "dist",

        // the server port
        // can also be written as a function, e.g.
        // port: function() { return 8282; }
        port: 8282,

        // the host ip address
        // If specified to, for example, "127.0.0.1" the server will
        // only be available on that ip.
        // Specify "0.0.0.0" to be available everywhere
        host: "0.0.0.0",

        cache: 0,
        showDir: true,
        autoIndex: true,

        // server default file extension
        ext: "html",

        // run in parallel with other tasks
        runInBackground: true,

        // specify a logger function. By default the requests are
        // sent to stdout.
        // logFn: function(req, res, error) { },

        // Proxies all requests which can't be resolved locally to the given url
        // Note this this will disable 'showDir'
        // proxy: "http://someurl.com",

        /// Use 'https: true' for default module SSL configuration
        /// (default state is disabled)
        // https: {
        //     cert: "cert.pem",
        //     key : "key.pem"
        // },

        // Tell grunt task to open the browser
        openBrowser: true

        // customize url to serve specific pages
        // customPages: {
        //     "/readme": "README.md",
        //     "/readme.html": "README.html"
        // }
      }
    },
    less: {
      development: {
        options: {
          paths: ["src/styles"]
        },
        files: {
          "dist/styles/app.css": "src/styles/app.less"
        }
      }
    },
    watch: {
      less: {
        files: "src/styles/*.less",
        tasks: "less",
        options: {
          spawn: false
        }
      },
      scripts: {
        files: "src/scripts/**/*.js",
        tasks: "browserify",
        options: {
          spawn: false
        }
      },
      copy: {
        files: ["src/*", "src/data/**", "src/images/**", "src/templates/**"],
        tasks: "copy",
        options: {
          spawn: false
        }
      }
    },
    browserify: {
      dist: {
        files: {
          // destination for transpiled js : source js
          "dist/scripts/app.js": "src/scripts/app.js"
        },
        options: {
          transform: [
            [
              "babelify",
              {
                presets: "es2015"
              }
            ]
          ],
          browserifyOptions: {
            debug: true
          }
        }
      }
    },
    copy: {
      main: {
        files: [
          {
            expand: true,
            cwd: "src/templates",
            src: "**",
            dest: "dist/templates",
            filter: "isFile"
          },
          {
            expand: true,
            cwd: "src/data",
            src: "**",
            dest: "dist/data",
            filter: "isFile"
          },
          {
            expand: true,
            cwd: "src/images",
            src: "**",
            dest: "dist/images",
            filter: "isFile"
          },
          {
            expand: true,
            cwd: "src",
            src: "index.html",
            dest: "dist",
            filter: "isFile"
          }
        ]
      }
    },
    ftpush: {
      build: {
        auth: {
          host: "205.134.253.202",
          port: 21,
          username: grunt.option("ftp-username"),
          password: grunt.option("ftp-pass")
        },
        src: "dist",
        dest: "/scorecardgenerator",
        keep: ["/screcardgenerator/images/**"],
        simple: false,
        useList: true
      }
    }
  });

  grunt.registerTask("build", ["browserify", "less", "copy"]);
  grunt.registerTask("serve", ["build", "http-server", "watch"]);
  grunt.registerTask("deploy", ["ftpush"]);
};
